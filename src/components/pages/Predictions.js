import React from 'react';
import axios from 'axios';
import './table.css';

export default class Predictions extends React.Component {
  state = {
    ticker: '',
  }

  handleChange = event => {
    this.setState({ [event.target.ticker]: event.target.value });
  }

        handleSubmit = event => {
            event.preventDefault();

    const user = {
      ticker: this.state.name,
    };

            axios.post(`https://localhost:8000/`, { user })
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                })
        }

        render() {
            return (
               <div class ="jumbotron temp2">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ticker</label>
                                <input type="text" class="form-control w-25 p-3" placeholder="Enter ticker" name = "ticker" onChange = { this.handleChange }/>
                            </div>         
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                        <br></br>
                        <table>
                            <tr>
                                <th>Positivity</th>
                                <th>Negativity</th>
                                <th>Neutrality</th>
                                <th>Predicted close</th>
                            </tr>
                            <tr>
                                <td>0.5</td>
                                <td>0.6</td>
                                <td>0.2</td>
                                <td>Negative</td>
                            </tr>
                        </table>
                </div>
                );
        }
}                