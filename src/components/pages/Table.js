import React from 'react';
import axios from 'axios';

export default class Table extends React.Component {
  state = {
    name: '',
    startdate: '',
    enddate: '',
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  }

        handleSubmit = event => {
            event.preventDefault();

    const user = {
      name: this.state.name,
      startdate: this.state.startdate,
      enddate: this.state.enddate,
    };

    axios.post(`http://127.0.0.1:8000/get_stocks/`, { name:this.state.name ,startdate:this.state.startdate ,enddate:this.state.enddate })
    .then(res => {
      console.log(res);
      console.log(res.data);
    })
  }

        render() {
            return ( 
               <div class ="jumbotron temp2">
               <form>
                <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control w-25 p-3" placeholder="Enter name" name = "name" onChange = { this.handleChange }/>
                </div>
                <div class="form-group">
                <label for="exampleInputPassword1">Start Date</label>
                <input type="date" class="form-control w-25 p-3" placeholder="date" name = "startdate" onChange = { this.handleChange }/>
                </div>
                <div class="form-group">
                <label for="exampleInputPassword1">End Date</label>
                <input type="date" class="form-control w-25 p-3" placeholder="date" name = "enddate" onChange = { this.handleChange }/>
                </div>               
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                <br></br>
                <table>
                  <tr>
                    <th>Date</th>
                    <th>Open</th>
                    <th>High</th>
                    <th>Low</th>
                    <th>Close</th>
                    <th>Adj close</th>
                    <th>Volume</th>
                  </tr>
                  <tr>
                    <td>05-05-21</td>
                    <td>350</td>
                    <td>400</td>
                    <td>340</td>
                    <td>370</td>
                    <td>360</td>
                    <td>2000</td>
                  </tr>
                </table>
                </div>
                );
        }
}                